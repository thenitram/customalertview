//
//  AlertView.m
//  Bubble Flags
//
//  Created by Martin on 3/19/18.
//  Copyright © 2018 Martin. All rights reserved.
//

#import "AlertView.h"

#define kFONT_1 @"CarterOne"
#define kFONT_2 @"HelveticaNeue-Medium"
#define kFONT_SIZE_BUTTON (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) ? 34 : 17
#define kFONT_SIZE_MESSAGE (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) ? 26 : 13
#define kFONT_SIZE_TITLE (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) ? 50 : 25

@implementation AlertView
NSLayoutConstraint *xConstraint;
NSLayoutConstraint *yConstraint;
float offScreenOffset;

#pragma mark- Lifecycle
- (id)initWithTitle:(NSString*)title message:(NSString *)message{
    self = [super init];
    if (self) {
        offScreenOffset = 100;

        titleText = title;
        messageText = message;
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self setupLayout];
        [self setupCloseButton];
        self.view.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.7f];
        
        backgroundTapGesture= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleBackgroundTap)];
        [self.view addGestureRecognizer:backgroundTapGesture];
    }
    return self;
}

- (void)dismissWithCompletion:(void (^ __nullable)(void))completion{
    __unsafe_unretained UIViewController *weakSelf = self;
    [self.view removeGestureRecognizer:backgroundTapGesture];
    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        mainView.alpha = 0;
        closeButton.alpha = 0;
        switch (_exitTransition) {
            case SCALE:
                mainView.transform = CGAffineTransformMakeScale(0.01, 0.01);
                break;
            case TOP:
                yConstraint.active = false;
                [mainView removeConstraint:yConstraint];
                yConstraint = [mainView.bottomAnchor constraintEqualToAnchor:self.view.topAnchor constant:-offScreenOffset];
                yConstraint.active = true;
                break;
            case BOTTOM:
                yConstraint.active = false;
                [mainView removeConstraint:yConstraint];
                yConstraint = [mainView.topAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:offScreenOffset];
                yConstraint.active = true;
                break;
            case LEFT:
                xConstraint.active = false;
                [mainView removeConstraint:xConstraint];
                xConstraint = [mainView.trailingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:-offScreenOffset];
                xConstraint.active = true;
                break;
            case RIGHT:
                xConstraint.active = false;
                [mainView removeConstraint:xConstraint];
                xConstraint = [mainView.leadingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:offScreenOffset];
                xConstraint.active = true;
                break;

        }
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [mainView removeConstraint:yConstraint];
        [mainView removeConstraint:xConstraint];
        mainView.transform = CGAffineTransformIdentity;
        [weakSelf dismissViewControllerAnimated:true completion:completion];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupButtonsLayout];
    [self setupConstraints];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    __unsafe_unretained UIViewController *weakSelf = self;
    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:.65 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        mainView.alpha = 1;
        if(willShowCloseButton){
            closeButton.alpha = 1;
        }
        switch (_enterTransition) {
            case SCALE:
                mainView.transform = CGAffineTransformIdentity;
                break;
            case TOP:
            case BOTTOM:
                yConstraint.active = false;
                [mainView removeConstraint:yConstraint];
                yConstraint = [mainView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor];
                yConstraint.active = true;
                break;
            case LEFT:
            case RIGHT:
                xConstraint.active = false;
                [mainView removeConstraint:xConstraint];
                xConstraint = [mainView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor];
                xConstraint.active = true;
                break;

        }
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf.view setUserInteractionEnabled:true];
    }];
}

- (void) dealloc{
    [listButtons removeAllObjects];
    [self removeAllSubviews:mainView];
}

#pragma mark-Private
- (void)setupLayout{
    listButtons = [NSMutableArray new];
    mainView = [[UIView alloc] init];
    mainView.layer.cornerRadius = 10.0f;
    mainView.layer.borderWidth = 1.5f;
    mainView.clipsToBounds = true;
    mainView.layer.borderColor = [UIColor whiteColor].CGColor;
    [mainView setBackgroundColor:[UIColor colorWithRed:0.8f green:0.8 blue:0.8 alpha:0.95]];
    [self.view addSubview:mainView];

    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc]initWithString:(titleText != nil)?titleText :@""];
    [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.1 green:0.1f blue:0.1f alpha:1] range:NSMakeRange(0, attr.length)];
    [attr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attr.length)];
    [attr addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFONT_1 size:kFONT_SIZE_TITLE] range:NSMakeRange(0, attr.length)];
    [attr addAttribute:NSStrokeColorAttributeName value:[UIColor colorWithRed:0.9f green:0.9f blue:0.9f alpha:1] range:NSMakeRange(0, attr.length)];
    [attr addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithInt:-3] range:NSMakeRange(0, attr.length)];

    titleLabel = [[UILabel alloc]init];
    titleLabel.numberOfLines = 1;
    titleLabel.adjustsFontSizeToFitWidth = true;
    titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    [titleLabel setAttributedText:attr];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    titleLabel.layer.shadowRadius = 2;
    titleLabel.layer.shadowOpacity = 0.8f;
    titleLabel.layer.shadowColor = [UIColor whiteColor].CGColor;
    titleLabel.layer.masksToBounds = false;
    [mainView addSubview:titleLabel];
    
    messageContentView = [UIView new];
    messageContentView.backgroundColor = [UIColor clearColor];
    [mainView addSubview:messageContentView];

    if(messageText != nil){
        messageLabel = [[UILabel alloc]init];
        messageLabel.text = messageText;
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        [messageLabel setFont:[UIFont fontWithName:kFONT_1 size:kFONT_SIZE_MESSAGE]];
        [messageContentView addSubview:messageLabel];
        
//        messageLabel.translatesAutoresizingMaskIntoConstraints = false;
//        [messageLabel.centerXAnchor constraintEqualToAnchor:messageContentView.centerXAnchor].active = true;
//        [messageLabel.topAnchor constraintEqualToAnchor:messageContentView.topAnchor constant:0].active = true;
//        [messageLabel.widthAnchor constraintEqualToAnchor:mainView.widthAnchor multiplier:0.9f].active = true;
//        [messageLabel.bottomAnchor constraintEqualToAnchor:messageContentView.bottomAnchor constant:0].active = true;
    }
    
    buttonContentView = [UIView new];
    buttonContentView.backgroundColor = [UIColor clearColor];
    [mainView addSubview:buttonContentView];
    
    footerContentView = [UIView new];
    footerContentView.backgroundColor = [UIColor clearColor];
    [mainView addSubview:footerContentView];
}

- (void)setupConstraints{
    [self.view setUserInteractionEnabled:false];
    mainView.translatesAutoresizingMaskIntoConstraints = false;
    switch (_enterTransition) {
        case SCALE:
            mainView.transform = CGAffineTransformMakeScale(0, 0);
            xConstraint = [mainView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor];
            xConstraint.active = true;
            yConstraint = [mainView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor];
            yConstraint.active = true;
            break;
        case TOP:
            //xPos is fixed
            xConstraint = [mainView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor];
            xConstraint.active = true;
            yConstraint = [mainView.bottomAnchor constraintEqualToAnchor:self.view.topAnchor constant:-offScreenOffset];
            yConstraint.active = true;
            break;
        case BOTTOM:
            //xPos is fixed
            xConstraint = [mainView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor];
            xConstraint.active = true;
            yConstraint = [mainView.topAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:offScreenOffset];
            yConstraint.active = true;
            break;
        case LEFT:
            //yPos is fixed
            xConstraint = [mainView.trailingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:-offScreenOffset];
            xConstraint.active = true;
            yConstraint = [mainView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor];
            yConstraint.active = true;
            break;
        case RIGHT:
            //yPos is fixed
            xConstraint = [mainView.leadingAnchor constraintEqualToAnchor:self.view.trailingAnchor];
            xConstraint.active = true;
            yConstraint = [mainView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor];
            yConstraint.active = true;
            break;
    }
    [mainView.widthAnchor constraintEqualToConstant:[self getAlertWidth]].active = true;
    
    [self setTitleLabelConstraint];
    [self setMessageContentViewConstraint];
    [self setupButtonContentViewConstraint];
    [self setupFooterContentViewConstraint];
}

- (void)setTitleLabelConstraint{
    if(titleLabel != nil){
        titleLabel.translatesAutoresizingMaskIntoConstraints = false;
        [titleLabel.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
        [titleLabel.topAnchor constraintEqualToAnchor:mainView.topAnchor].active = true;
        [titleLabel.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
        //use intrinsinc height for title
    }
}

- (void)setMessageContentViewConstraint{
    if(messageContentView != nil){
        UIView *separatorLine = [UIView new];
        separatorLine.backgroundColor = [UIColor whiteColor];
        [mainView addSubview:separatorLine];
        bool hasTitle = (titleLabel.attributedText.length > 0); //hasTitle
        separatorLine.translatesAutoresizingMaskIntoConstraints = false;
        [separatorLine.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
        [separatorLine.topAnchor constraintEqualToAnchor:titleLabel.bottomAnchor].active = true;
        [separatorLine.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
        [separatorLine.heightAnchor constraintEqualToConstant:(hasTitle)?1:0].active = true;
        
        messageContentView.translatesAutoresizingMaskIntoConstraints = false;
        [messageContentView.topAnchor constraintEqualToAnchor:separatorLine.bottomAnchor].active = true;
        [messageContentView.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
        [messageContentView.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
        UIView *view = [[messageContentView subviews]lastObject];
        if (view != nil){
            view.translatesAutoresizingMaskIntoConstraints = false;
            [view.leadingAnchor constraintEqualToAnchor:messageContentView.leadingAnchor constant:5].active = true;
            [view.trailingAnchor constraintEqualToAnchor:messageContentView.trailingAnchor constant:-5].active = true;
            [view.topAnchor constraintEqualToAnchor:messageContentView.topAnchor constant:5].active = true;
            [view.bottomAnchor constraintEqualToAnchor:messageContentView.bottomAnchor constant:-10].active = true;
        }
    }
}

- (void)setupButtonContentViewConstraint{
    UIView *separatorLine = [UIView new];
    separatorLine.backgroundColor = [UIColor whiteColor];
    [mainView addSubview:separatorLine];
    bool hasMessageContent = [messageContentView subviews].count > 0; //separator + actual content
    separatorLine.translatesAutoresizingMaskIntoConstraints = false;
    [separatorLine.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
    [separatorLine.topAnchor constraintEqualToAnchor:messageContentView.bottomAnchor].active = true;
    [separatorLine.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
    [separatorLine.heightAnchor constraintEqualToConstant:(hasMessageContent)?0:0].active = true;

    buttonContentView.translatesAutoresizingMaskIntoConstraints = false;
    [buttonContentView.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
    [buttonContentView.topAnchor constraintEqualToAnchor:separatorLine.bottomAnchor].active = true;
    [buttonContentView.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
    [buttonContentView.bottomAnchor constraintEqualToAnchor:footerContentView.topAnchor].active = true;
}

- (void)setupFooterContentViewConstraint{

    footerContentView.translatesAutoresizingMaskIntoConstraints = false;
    [footerContentView.centerXAnchor constraintEqualToAnchor:mainView.centerXAnchor].active = true;
    [footerContentView.widthAnchor constraintEqualToAnchor:mainView.widthAnchor].active = true;
    [footerContentView.bottomAnchor constraintEqualToAnchor:mainView.bottomAnchor].active = true;
//    [footerContentView.heightAnchor constraintEqualToConstant:100].active = true;
//no height
}

- (void)canTapBackgroundToDismiss:(BOOL)flag{
    canTapBackgroundToDismiss = flag;
}

- (void)handleBackgroundTap{
    if(canTapBackgroundToDismiss){
        [self handleCloseButtonPressed];
    }
}

- (void)handleCloseButtonPressed{
    [self dismissWithCompletion:nil];
}

- (void)setupButtonsLayout{
    //remove all buttons
    for (UIButton *button in listButtons){
        if([[buttonContentView subviews] containsObject:button]){
            [button removeConstraints:button.constraints];
            [button removeFromSuperview];
        }
    }

    if((listButtons.count == 2 && _axis == HORIZONTAL) || _axis == HORIZONTAL){
        [self setupButtonsHorizontally];
    }
    else if(_axis == VERTICAL){
        [self setupButtonsVertically];
    }
}

- (void)setupCloseButton{
    if (closeButton == nil){
        //close button
        closeButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [closeButton setContentMode:UIViewContentModeScaleAspectFit];
        [closeButton setBackgroundImage:[UIImage imageNamed:@"image_buttonClose"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(handleCloseButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:closeButton];

        //constraint
        float size = [self getAlertWidth]*0.15;
        closeButton.translatesAutoresizingMaskIntoConstraints = false;
        [closeButton.trailingAnchor constraintEqualToAnchor:mainView.trailingAnchor constant:size/2].active = true;
        [closeButton.topAnchor constraintEqualToAnchor:mainView.topAnchor constant:-size/2].active = true;
        [closeButton.widthAnchor constraintEqualToConstant:size].active = true;
        [closeButton.heightAnchor constraintEqualToConstant:size].active = true;
        closeButton.alpha = 0;
    }
}

- (void)setupButtonsHorizontally{
    int buttonWidth =  [self getAlertWidth] / listButtons.count;
    float estimatedHeight = [self getAlertWidth] * 0.18f;

    for (int x=0; x<listButtons.count; x++){
        UIButton *button = listButtons[x];
        UIView *lastObject = [buttonContentView subviews].lastObject;
        [buttonContentView addSubview:button];
        button.translatesAutoresizingMaskIntoConstraints = false;
        if(x == 0){
            [button.topAnchor constraintEqualToAnchor:buttonContentView.topAnchor constant:3].active = true;
        }
        else{
            [button.centerYAnchor constraintEqualToAnchor:lastObject.centerYAnchor].active = true;
        }
        [button.leadingAnchor constraintEqualToAnchor:buttonContentView.leadingAnchor constant:(buttonWidth * x)].active = true;
        [button.heightAnchor constraintEqualToConstant:(button.frame.size.height == 0) ?estimatedHeight :button.frame.size.height].active = true;
        [button.widthAnchor constraintEqualToConstant:buttonWidth].active = true;
        [button.bottomAnchor constraintEqualToAnchor:buttonContentView.bottomAnchor constant:-5].active = true;
    }
}

- (void)setupButtonsVertically{
//    int buttonWidth =  [self getAlertWidth] * .75;
    float estimatedHeight = [self getAlertWidth] * 0.18f;

    for (int x=0; x<listButtons.count; x++){
        UIButton *button = listButtons[x];
        UIView *lastObject = [buttonContentView subviews].lastObject;
        [buttonContentView addSubview:button];
        
        if(x == 0){
            [button.topAnchor constraintEqualToAnchor:buttonContentView.topAnchor constant:5].active = true;
        }
        else{
            [button.topAnchor constraintEqualToAnchor:lastObject.bottomAnchor constant:3].active = true;
        }
        
        button.translatesAutoresizingMaskIntoConstraints = false;
        [button.centerXAnchor constraintEqualToAnchor:buttonContentView.centerXAnchor].active = true;
        [button.heightAnchor constraintEqualToConstant:(button.frame.size.height == 0) ?estimatedHeight :button.frame.size.height].active = true;
        [button.widthAnchor constraintEqualToConstant:[self getAlertWidth]].active = true;

        if(x == listButtons.count -1){
            [button.bottomAnchor constraintEqualToAnchor:buttonContentView.bottomAnchor constant:-8].active = true;
        }
    }
}

#pragma mark- Public
- (CGFloat)getAlertWidth{
//    float multiplier = [[UIScreen mainScreen] bounds].size.width / [[UIScreen mainScreen] bounds].size.height;
    return self.view.frame.size.width * .8;
}

- (void)showCloseButton{
    willShowCloseButton = true;
}

- (void)hideCloseButton{
    willShowCloseButton = false;
}

- (void)addButton:(UIButton *)button{
    if([listButtons containsObject:listButtons]){
        return;
    }
    [listButtons addObject:button];
    //    [self setupButtonsLayout];
}

- (void)addMessageView:(UIView *)view{
    [self removeAllSubviews:messageContentView];
    [messageContentView removeConstraints:messageContentView.constraints];
    [messageContentView addSubview:view];

    //set constraint
    view.frame = (CGRect){.origin=CGPointMake([self getAlertWidth]/2-(view.frame.size.width)/2, 0), .size=view.frame.size};
    [self setMessageContentViewConstraint];
}

- (void)addFooterView:(UIView *)view{
    [footerContentView addSubview:view];

    view.translatesAutoresizingMaskIntoConstraints = false;
    [view.centerXAnchor constraintEqualToAnchor:footerContentView.centerXAnchor].active = true;
    [view.topAnchor constraintEqualToAnchor:footerContentView.topAnchor].active = true;
    [view.bottomAnchor constraintEqualToAnchor:footerContentView.bottomAnchor].active = true;
    [view.widthAnchor constraintEqualToAnchor:footerContentView.widthAnchor].active = true;
}

-(void)removeAllSubviews:(UIView *)view{
    while(view.subviews.count>0){
        UIView *subView = [view.subviews objectAtIndex:0];
        [self removeAllSubviews:subView];
        
        [subView removeFromSuperview];
        subView = nil;
    }
}
@end
