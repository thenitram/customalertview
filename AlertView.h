//
//  AlertView.h
//  Bubble Flags
//
//  Created by Martin on 3/19/18.
//  Copyright © 2018 Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Transition) {
    SCALE,
    TOP,
    BOTTOM,
    LEFT,
    RIGHT,
};

typedef NS_ENUM(NSInteger, Axis) {
    VERTICAL,
    HORIZONTAL,
};

@interface AlertView : UIViewController{
    NSString *titleText;
    NSString *messageText;
    UIView *mainView;
    UIView *buttonContentView;
    UIView *messageContentView;
    UIView *footerContentView;
    NSMutableArray *listButtons;
    UILabel *titleLabel;
    UILabel *messageLabel;
    UIButton *closeButton;
    bool willShowCloseButton;
    bool canTapBackgroundToDismiss;
    UITapGestureRecognizer *backgroundTapGesture;
    
}
@property (nonatomic, assign) Transition enterTransition;
@property (nonatomic, assign) Transition exitTransition;
@property (nonatomic, assign) Axis axis;

- (id)initWithTitle:(NSString*_Nullable)title message:(NSString *_Nullable)message;
- (void)addButton:(UIButton *_Nonnull)button;
- (CGFloat)getAlertWidth;
- (void)dismissWithCompletion:(void (^ __nullable)(void))completion;
- (void)showCloseButton;
- (void)hideCloseButton;
- (void)addMessageView:(UIView *)view;
- (void)addFooterView:(UIView *)view;
- (void)canTapBackgroundToDismiss:(BOOL)flag;
@end
